package applications

import (
	"cwtch.im/tapir"
	"cwtch.im/tapir/primitives/core"
)

// TranscriptApp defines a Tapir Meta-App which provides a global cryptographic transcript
type TranscriptApp struct {
	transcript *core.Transcript
}

// NewInstance creates a new TranscriptApp
func (TranscriptApp) NewInstance() tapir.Application {
	ta := new(TranscriptApp)
	return ta
}

// Init initializes the cryptographic transcript
func (ta *TranscriptApp) Init(connection tapir.Connection) {
	ta.transcript = core.NewTranscript("tapir-transcript")
}

// Transcript returns a pointer to the cryptographic transcript
func (ta *TranscriptApp) Transcript() *core.Transcript {
	return ta.transcript
}

// PropagateTranscript overrides the default transcript and propagates a transcript from a previous session
func (ta *TranscriptApp) PropagateTranscript(transcript *core.Transcript) {
	ta.transcript = transcript
}
