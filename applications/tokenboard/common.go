package tokenboard

import (
	"cwtch.im/tapir/primitives/auditable"
	"cwtch.im/tapir/primitives/privacypass"
)

// AppHandler allows clients to react to specific events.
type AppHandler interface {
	HandleNewMessages(previousLastCommit []byte)
}

// MessageType defines the enum for TokenBoard messages
type messageType int

const (
	replayRequestMessage messageType = iota
	replayResultMessage
	postRequestMessage
	postResultMessage
)

// Message encapsulates the application protocol
type Message struct {
	MessageType   messageType
	PostRequest   postRequest   `json:",omitempty"`
	PostResult    postResult    `json:",omitempty"`
	ReplayRequest replayRequest `json:",omitempty"`
	ReplayResult  replayResult  `json:",omitempty"`
}

// ReplayRequest requests a reply from the given Commit
type replayRequest struct {
	LastCommit []byte
}

// PostRequest requests to post the message to the board with the given token
type postRequest struct {
	Token   privacypass.SpentToken
	Message auditable.Message
}

// PostResult returns the success of a given post attempt
type postResult struct {
	Success bool
	Proof   auditable.SignedProof
}

// ReplayResult is sent by the server before a stream of replayed messages
type replayResult struct {
	NumMessages int
}
