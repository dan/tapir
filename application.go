package tapir

import (
	"cwtch.im/tapir/primitives/core"
)

// Capability defines a status granted to a connection, from an application. That allows the connection to access
// other Application or functions within an Application.
type Capability string

// Application defines the interface for all Tapir Applications
type Application interface {
	NewInstance() Application
	Init(connection Connection)
	Transcript() *core.Transcript
	PropagateTranscript(transcript *core.Transcript)
}

// InteractiveApplication defines the interface for interactive Tapir applications (apps that expect the user to send
// and receive messages from)
type InteractiveApplication interface {
	Application
	Listen()
}
