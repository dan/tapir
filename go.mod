module cwtch.im/tapir

require (
	git.openprivacy.ca/openprivacy/libricochet-go v1.0.4
	github.com/gtank/merlin v0.1.1
	github.com/gtank/ristretto255 v0.1.2
	go.etcd.io/bbolt v1.3.3
	golang.org/x/crypto v0.0.0-20190513172903-22d7a77e9e5f
	golang.org/x/sync v0.0.0-20190423024810-112230192c58 // indirect
)

go 1.13
