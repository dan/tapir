package persistence

import (
	"testing"
)

func TestBoltPersistence_Open(t *testing.T) {
	var db Service
	db = new(BoltPersistence)
	db.Open("test.dbgi")
	db.Setup([]string{"tokens"})
	db.Persist("tokens", "random_value", true)

	var exists bool
	db.Load("tokens", "random_value", &exists)

	if exists {
		t.Logf("Successfully stored: %v", exists)
	} else {
		t.Fatalf("Failure to store record in DB!")
	}
	db.Close()
}
