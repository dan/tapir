package primitives

import (
	"testing"
)

func TestIdentity_EDH(t *testing.T) {

	id1, _ := InitializeEphemeralIdentity()
	id2, _ := InitializeEphemeralIdentity()

	k1 := id1.EDH(id2.PublicKey())
	k2 := id2.EDH(id1.PublicKey())

	t.Logf("k1: %x\nk2: %x\n", k1, k2)

}
